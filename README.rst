====
soap
====


.. image:: https://img.shields.io/pypi/v/soap.svg
        :target: https://pypi.python.org/pypi/soap

SOAP: Applying the Viterbi algorithm to search for sources
of continuous gravitational waves.

.. image:: https://git.ligo.org/joseph.bayley/soapcw/-/raw/master/pipeline/images/vitmap_ex.png



!!!!!! This Project has moved location !!!!!!
----------------
**Please use the github link here: https://github.com/jcbayley/soapcw **

SOAP is primarily developed to search for continuous sources of
gravitational waves, however, has a more general application to search
for and long duration weak signal.

This package also includes tools to load in standard short Fourier transforms (SFTs) and prepare them for usage with the core SOAP search.


* Free software: MIT license
* Documentation: https://joseph.bayley.docs.ligo.org/soapcw

Publications
----------------
* Methods paper: https://journals.aps.org/prd/abstract/10.1103/PhysRevD.100.023006
* CNN followup paper: https://journals.aps.org/prd/abstract/10.1103/PhysRevD.102.083024
* Parameter estimation paper: https://journals.aps.org/prd/abstract/10.1103/PhysRevD.106.083022 


Features
----------------


TODO
----------------
* robustly include three detectors 

Credits
----------------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
